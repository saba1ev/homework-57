import React from 'react';
import './Dask.css';

const Dask = (props) =>{
  console.log(props);
  return(
   props.list.map((lists) =>{
     return(
       <div className='Dask'>
         <h5>{lists.name}</h5>
         <p>{lists.price} KGS</p>
         <button onClick={() => props.click(lists.name, lists.price)}>X</button>
       </div>
     )
  }))
};

export default Dask;