import React from 'react';
import './OrderEntry.css';

const OrderEntry = (props) =>{
  return(
    <div className='Form'>
      <input type="text" placeholder='Item name' value={props.val} onChange={props.change}/>
      <input type="text" placeholder='Cost' value={props.value} onChange={props.onchange}/>
      <p>KGS</p>
      <button onClick={() => props.add()}>add</button>
    </div>
  )
};

export default OrderEntry