import React, { Component } from 'react';

import './App.css';
import OrderEntry from "./components/OrderEntry/OrderEntry";
import Dask from "./components/Dask/Dask";

class App extends Component {
  state ={
    list: [],
    itemName: '',
    cost: '',
    totalPrice: 0,
  };
  changeHendler = (event) =>{
    this.setState({
      itemName: event.target.value
    })
  };
  changePrice = (event) =>{
    this.setState({
      cost: event.target.value
    })
  };
  getTotalPrice = (array) =>{
    const totalPrice = array.reduce((total, task)=>{
      return total += parseInt(task.price);
    }, 0);
    this.setState({
      totalPrice: totalPrice
    })
  };
  AddItem = () =>{
    const copyList = [...this.state.list];
    const newList = {name: this.state.itemName, price: this.state.cost};

    copyList.push(newList);
    this.getTotalPrice(copyList);
    this.setState({list: copyList, itemName: '', cost: "",})
  };
  removeItem = (name, price) =>{
    const copyList = [...this.state.list];
    let total = this.state.totalPrice;
    const index = copyList.findIndex((item)=> item.name === name);

    copyList.splice(index, 1);
    total -= parseInt(price);

    this.setState({list: copyList, totalPrice: total});
  };
  render() {
    return (
      <div className="App">
        <OrderEntry change={(event)=>this.changeHendler(event)}
                    onchange={(event) => this.changePrice(event)}
                    val={this.state.itemName}
                    value={this.state.cost}
                    add={() => this.AddItem()}
        />
        <Dask list={this.state.list}
              click={(name, price)=> this.removeItem(name, price)}/>
        <span>Общая сумма: {this.state.totalPrice}</span>
      </div>
    );
  }
}

export default App;
