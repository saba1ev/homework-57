const tasks = [
  {id: 234, title: 'Create user registration API', timeSpent: 4, category: 'Backend', type: 'task'},
  {id: 235, title: 'Create user registration UI', timeSpent: 8, category: 'Frontend', type: 'task'},
  {id: 237, title: 'User sign-in via Google UI', timeSpent: 3.5, category: 'Frontend', type: 'task'},
  {id: 238, title: 'User sign-in via Google API', timeSpent: 5, category: 'Backend', type: 'task'},
  {id: 241, title: 'Fix account linking', timeSpent: 5, category: 'Backend', type: 'bug'},
  {id: 250, title: 'Fix wrong time created on new record', timeSpent: 1, category: 'Backend', type: 'bug'},
  {id: 262, title: 'Fix sign-in failed messages', timeSpent: 2, category: 'Frontend', type: 'bug'},
];

const frontendTimeSpent = tasks.reduce((time, task) => {
  if (task.category === "Frontend") time += task.timeSpent;
    return time;
}, 0);

const bugTimeSpent =tasks.reduce((time, task)=>{
  if (task.type === 'bug') time += task.timeSpent;
    return time
}, 0);

const countTaskUI = tasks.filter((task)=>{
  return task.title.includes('UI')
});

const countWork = tasks.reduce((accum, task) => {
  if(task.category === 'Frontend') {
    accum.Frontend++
  } else if (task.category === 'Backend') {
    accum.Backend++
  }
  return accum
}, {Frontend: 0, Backend: 0});

const timeSpendFour = tasks.reduce((accum, task)=>{
  if (task.timeSpent >= 4){
    accum.push({title: task.title, category: task.category })
  }
  return accum
}, []);

console.log('Время затраченое на "Frontend": ' + frontendTimeSpent + " Часов");
console.log('Время затраченое на "bug": ' + bugTimeSpent + " Часов");
console.log('Количество задач с "UI": ' + countTaskUI.length + ' Задач');
console.log(countWork);
console.log(timeSpendFour);